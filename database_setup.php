<?php
// MySQL Server Connection Parameters
$servername = "localhost";
$username = "username";
$password = "password";

// Name of the database i.e., to be created for Alerting System
$dbname = "AlertSystem";

// Connect with MySQL Server
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Create database
$sql = "CREATE DATABASE $dbname";

if ($conn->query($sql) === TRUE) {
	// If database creation successful..
    echo "Database created successfully";
    $conn->close();

    echo "Creating connection with newly created database...";
    
    // Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	echo "Creating table Alert for storing alerts...";
	
	// sql to create table for Alerts
	$sql = "CREATE TABLE  `Alert`(
		`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		`reference_id` VARCHAR(30) NOT NULL,
		`delay` UNSIGNED SMALLINT NOT NULL,
		`description` VARCHAR(50) NOT NULL,
		`listed` TINYINT(1) NOT NULL
	)";

	if ($conn->query($sql) === TRUE) {
	    echo "Table Alert created successfully..";
	} else {
	    echo "Error creating table: " . $conn->error;
	}

	// Close Connection
	$conn->close();

} 
else {
	// If database creation failed..
    echo "Error creating database: " . $conn->error;
    
    // Close Connection
    $conn->close();
}
?>