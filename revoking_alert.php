<?php
	//Extract parameters passed in variables
	if(count($_POST)>0 and array_key_exists('reference_id', $_POST)){
		// If POST method is used
		$reference_id = $_POST['reference_id'];
	}
	else if(count($_GET)>0 and array_key_exists('reference_id', $_GET)){
		// If GET method is used
		$reference_id = $_GET['reference_id'];
	}
	else{
		// If parameters not passed, return error
		$response = array("Error"=>"Alert Not Revoked","Error Description"=>"No reference_id Passed");
		die(json_encode($response));
	}

	// Database Connection Parameters
	$servername = "localhost";
	$username = "username";
	$password = "password";
	$dbname = "AlertSystem";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

	// Check connection
	if ($conn->connect_error) {
	    $response = array("Error"=>"Alerts can't be revoked","Error Description"=>"Unable to connect with database\n".$conn->connect_error);
		die(json_encode($response));
	}

	// Revoke an alert which is already listed
	$sql = "DELETE FROM `Alert` WHERE `reference_id`=$reference_id";
	$conn->query($sql);

	// Close connection
	$conn->close();
?>