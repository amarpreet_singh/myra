<?php

// Database Connection Parameters
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "AlertSystem";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    $response = array("Error"=>"Alerts can't be listed","Error Description"=>"Unable to connect with database\n".$conn->connect_error);
	die(json_encode($response));
} 

// Read all the alerts that have happened in the system and not yet been cleared
$sql = "SELECT `reference_id`, `delay`, `description` FROM `Alert` WHERE `listed` = 1";
$result = $conn->query($sql);
$alerts = $result->fetch_all(MYSQLI_ASSOC);

// Close connection
$conn->close();

// Output alerts
$alerts = array("alerts"=>$alerts);
die(json_encode($alerts));

?>