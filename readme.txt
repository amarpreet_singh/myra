The tech stack that has been used for this project are PHP and MySQL.
The reason for choosing these stacks are - 
PHP - 
	Whether it is Windows, MacOS, Linux or UNIX; it supports all the major web browsers.
	PHP also supports all the major web servers; be it Apache or Microsoft IIS. It also supports Netscape and personal web server.
	PHP uses its own memory, so the workload of the server and loading time gets reduced automatically, which results into the faster processing speed. 
	It is one of the most secured way of developing websites and web applications; as it has got a security layer to protect against viruses and threats.

MySQL
	MySQL is very easy to install, and thanks to a bevy of third-party tools that can be added to the database, setting up an implementation is a relatively simple task.
	It remains one of the most-used database systems in the world. It’s compatible with virtually every operating system, and is more or less an industry standard.