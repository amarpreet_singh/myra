<?php
	//Extract parameters passed in variables
	if(count($_POST)>0 and array_key_exists('reference_id', $_POST) and array_key_exists('delay', $_POST) and array_key_exists('description', $_POST)){
		// If POST method is used
		$reference_id = $_POST['reference_id'];
		$delay = $_POST['delay'];
		$description = $_POST['description'];
	}
	else if(count($_GET)>0 and array_key_exists('reference_id', $_GET) and array_key_exists('delay', $_GET) and array_key_exists('description', $_GET)){
		// If GET method is used
		$reference_id = $_GET['reference_id'];
		$delay = $_GET['delay'];
		$description = $_GET['description'];
	}
	else{
		// If parameters not passed, return error
		$response = array("Error"=>"Alert Not Posted","Error Description"=>"No Parameters Passed/ Not all parameters passed");
		die(json_encode($response));
	}

	// Database Connection Parameters
	$servername = "localhost";
	$username = "username";
	$password = "password";
	$dbname = "AlertSystem";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
	    $response = array("Error"=>"Alert Not Posted","Error Description"=>"Unable to connect with database\n".$conn->connect_error);
	    die(json_encode($response));
	} 

	// Insert alert
	$sql = "INSERT INTO Alert (reference_id,delay,description,listed)
	VALUES ('$reference_id',$delay,'$description',0)";

	// Check if alert inserted
	if ($conn->query($sql) === FALSE) {
		//If alert insertion failed, return error
	    $response = array("Error"=>"Alert Not Posted","Error Description"=>"Unable to insert alert in database");
	    die(json_encode($response));
	}

	// Close Connection
	$conn->close();

	// Wait for seconds passed in delay parameter as the alert should appear in the listing API after the seconds passed in delay
	sleep((int)$delay);

	// Database Connection Parameters
	$servername = "localhost";
	$username = "username";
	$password = "password";
	$dbname = "AlertSystem";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
	    $response = array("Error"=>"Alert Not Posted","Error Description"=>"Unable to connect with database\n".$conn->connect_error);
	    die(json_encode($response));
	} 

	// Allow alert to get listed
	$sql = "UPDATE `Alert` SET `listed`=1 WHERE `reference_id = '$reference_id'";
	if ($conn->query($sql) === TRUE) {
		//If alert is allowed to get listed, return alert json
		$response = array("alert"=>array("reference_id":$reference_id,"delay":$delay,"description":$description));
	    die(json_encode($response));
	    
	} else {
		//If failed, return error
	    $response = array("Error"=>"Alert Not Posted","Error Description"=>"Unable to insert alert in database");
	    die(json_encode($response));
	}

	// Close Connection
	$conn->close();
?>